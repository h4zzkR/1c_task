#! /usr/bin/env python
from app import app
from app.helpers import create_default_admin

create_default_admin()
app.run(debug=True, host="0.0.0.0", port=8080)
