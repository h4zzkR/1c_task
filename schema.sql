DROP TABLE IF EXISTS users;
DROP TABLE IF EXISTS records;

CREATE TABLE users (
    id integer primary key autoincrement,
    name string not null,
    login string not null,
    password string not null,
    authenticated boolean default false,
    is_admin boolean not null default false
);

CREATE TABLE records (
    id_ integer primary key autoincrement,
    id integer,
    coffee_type string not null,
    timestamp string not null,

    FOREIGN KEY (id)
    REFERENCES users (id)
       ON UPDATE RESTRICT
       ON DELETE RESTRICT
);