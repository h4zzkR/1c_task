from app import db
from sqlalchemy.sql import func
from sqlalchemy.orm import relationship


class User(db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255))

    login = db.Column(db.String(255), unique=True)
    password = db.Column(db.String)
    authenticated = db.Column(db.Boolean, default=False)
    is_admin = db.Column(db.Boolean)

    # Next methods are for login_manager:
    def is_active(self):
        return True

    def get_id(self):
        return self.id

    @property
    def is_authenticated(self):
        return self.authenticated

    @property
    def is_anonymous(self):
        return False


class Record(db.Model):
    __tablename__ = "records"
    id_ = db.Column(db.Integer, primary_key=True)
    id = db.Column(db.Integer, db.ForeignKey('users.id'))
    coffee_type = db.Column(db.String(255))
    timestamp = db.Column(db.DateTime(timezone=False), server_default=func.now())

    user = relationship('User', backref='records')

    def __init__(self, id, coffee_type: str):
        self.id = id
        self.coffee_type = coffee_type
        self.timestamp = func.now()
