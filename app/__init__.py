from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager

import os, json

basedir = os.path.abspath(os.path.dirname(__file__))

app = Flask(__name__)
app.config['SECRET_KEY'] = 'lorem ipsum'
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///mydatabase.db'

db = SQLAlchemy(app)

login_manager = LoginManager()
login_manager.init_app(app)

app.config.from_object(__name__)

from app import views

# with open('../password.json') as f:
#     SETTINGS = json.load(f)
