from app import app, db, login_manager
import datetime
from flask import render_template, request, redirect, url_for
from app.forms import UserForm, RecordForm, LoginForm, FilterForm
from app.models import User, Record

import flask_bcrypt as bcrypt
from flask_login import login_user, current_user, login_required, logout_user
from app.helpers import getByLogin, admin, create_user


"""
Сюда можно добавлять rout-ы, @admin - декоратор для доступа к секции только администраторам
"""

@login_manager.user_loader
def user_loader(user_id):
    return db.session.query(User).get(user_id)


@app.route('/')
def home():
    if not current_user.is_authenticated:
        return redirect(url_for('login'))
    else:
        return render_template('home.html')


@app.route('/users')
@login_required
@admin
def show_users():
    users = db.session.query(User).all()
    return render_template('show_users.html', users=users)


@app.route('/records')
@login_required
@admin
def show_records():
    filter_login = request.args.get('by_login')
    filter_coffee = request.args.get('by_coffee')
    filter_login = (None if filter_coffee == '' else filter_login)
    filter_coffee = (None if filter_coffee == '' else filter_coffee)

    if filter_coffee and filter_coffee:
        records = db.session.query(Record).filter(
            Record.coffee_type == filter_coffee and Record.user.login == filter_login)
    elif filter_login:
        records = db.session.query(Record).filter(Record.user.login == filter_login)
    elif filter_coffee:
        records = db.session.query(Record).filter(Record.coffee_type == filter_coffee)
    else:
        records = db.session.query(Record).all()

    print(filter_coffee, filter_login)
    return render_template('show_records.html', records=records)


@app.route("/login", methods=["GET", "POST"])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        login = form.login.data
        user = getByLogin(login)
        if user is None:
            return render_template('404.html'), 404

        # securely compare passwords
        if bcrypt.check_password_hash(user.password, form.password.data):
            user.authenticated = True
            db.session.add(user)
            db.session.commit()
            login_user(user, remember=True)
            return redirect(url_for('show_records'))

    return render_template("login.html", form=form)


@app.route("/logout", methods=["GET"])
@login_required
def logout():
    """Logout the current user."""
    user = current_user
    user.authenticated = False
    db.session.add(user)
    db.session.commit()
    logout_user()
    return redirect(url_for('home'))


@app.route('/add-user', methods=['POST', 'GET'])
@login_required
@admin
def add_user():
    user_form = UserForm()
    if request.method == 'POST':
        if user_form.validate_on_submit():
            create_user(user_form.name.data, user_form.login.data,
                        user_form.password.data, False)

            return redirect(url_for('show_users'))

    return render_template('add_user.html', form=user_form)


@app.route('/add-record', methods=['POST', 'GET'])
@login_required
def add_record():
    record_form = RecordForm()
    if request.method == 'POST':
        if record_form.validate_on_submit():
            timest = datetime.datetime.now()
            coffees = record_form.coffee_type.data + ','
            for coffee in coffees.split(',')[:-1]:
                record = Record(current_user.id, coffee.replace(' ', ''))
                db.session.add(record)
            db.session.commit()

            return redirect(url_for('add_record'))

    return render_template('add_record.html', form=record_form)


@app.after_request
def add_header(response):
    response.headers['X-UA-Compatible'] = 'IE=Edge,chrome=1'
    response.headers['Cache-Control'] = 'public, max-age=600'
    return response


@app.errorhandler(404)
def page_not_found(error):
    return render_template('404.html'), 404


if __name__ == '__main__':
    app.run(debug=True, host="0.0.0.0", port="8080")
