from flask import render_template, request, redirect, url_for
from app.forms import UserForm, RecordForm
from app.models import User, Record
from app import app, db
from app.models import User, Record


class Cashier:
    def __init__(self, uid):
        self.cashier = uid


class Admin:
    def __init__(self, uid):
        got_users = db.session.query(User).filter(User.id == uid)
        if not got_users[0].is_admin:
            raise RuntimeError
