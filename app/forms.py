from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms.widgets import CheckboxInput
from wtforms.validators import InputRequired


class RecordForm(FlaskForm):
    coffee_type = StringField('Coffee', validators=[InputRequired()])

class LoginForm(FlaskForm):
    login = StringField('Login', validators=[InputRequired()])
    password = StringField('Password', validators=[InputRequired()])

class UserForm(FlaskForm):
    name = StringField('Name', validators=[InputRequired()])
    login = StringField('Login', validators=[InputRequired()])
    password = StringField('Password', validators=[InputRequired()])
    is_admin = CheckboxInput()

class FilterForm(FlaskForm):
    by_login = StringField('Login', validators=[InputRequired()], default="0")
    by_coffee = StringField('Coffee', validators=[InputRequired()], default="0")
