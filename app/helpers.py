from app import app
from app.models import User, db
from getpass import getpass
import flask_bcrypt as bcrypt

from functools import wraps
from flask import render_template
from flask_login import login_user, current_user, login_required


def getByLogin(login):
    user = db.session.query(User).filter(User.login == login)
    if user.count() == 0:
        return None
    return user[0]


def admin(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if not current_user.is_admin:
            return render_template('404.html'), 404

        return f(*args, **kwargs)

    return decorated_function


def create_user(name, login, password, is_admin):
    user = User(
        login=login,
        password=bcrypt.generate_password_hash(password),
        is_admin=is_admin,
        name=name
    )

    db.session.add(user)
    db.session.commit()


def create_default_admin():
    """
    Создает админа по умолчанию для удобства демонстрации.
    Только админы могут создавать новых пользователей.
    """
    with app.app_context():
        login = "admin0"
        if getByLogin(login) is not None:
            return  # already created

        password = getpass()
        assert password == getpass('Password (again):')

        user = User(
            login=login,
            password=bcrypt.generate_password_hash(password),
            is_admin=True,
            name=login,
        )

        db.session.add(user)
        db.session.commit()

        print("Admin created")
